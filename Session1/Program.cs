﻿/*using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Session1
{
    class Program
    {
        static void Main(string[] args)
        {
            MessageBox.Show("Message", "Title",
         MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

        }
    }
}
*/

//Task 4

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Starting BMI Program....");
            string name;
            string exit = "";
            do
            {
                Console.WriteLine("Enter your name: ");
                name = Console.ReadLine();
                Console.WriteLine("Enter weight in kilograms: ");
                string weight = Console.ReadLine();
                Console.WriteLine("Enter height in meters: ");
                string height = Console.ReadLine();
                Console.WriteLine("Enter M for male and F for female:");
                string gender = Console.ReadLine().ToUpper();

                float float_weight = Single.Parse(weight);
                float float_height = Single.Parse(height);

                float bmi = (float_weight) / (float_height * float_height);

                string result = "";

                if (gender.Equals("F"))
                {
                    if (bmi < 17.5) { result = "Anorexia"; }
                    else if (bmi < 19.1) { result = "Underweight"; }
                    else if (bmi > 19.1 && bmi < 25.8) { result = "In normal range"; }
                    else if (bmi > 25.8 && bmi < 27.3) { result = "Marginally overweight"; }
                    else if (bmi > 27.3 && bmi < 32.3) { result = "Overweight"; }
                    else if (bmi > 32.3 && bmi < 35) { result = "Very Overweight or obese"; }
                    else if (bmi > 35 && bmi < 40) { result = "Severely Obese"; }
                    else if (bmi > 40 && bmi < 50) { result = "Morbidly Obese"; }
                    else { result = "Super Obese"; }

                }
                if (gender.Equals("M"))
                {
                    if (bmi < 17.5) { result = "Anorexia"; }
                    else if (bmi < 20.7) { result = "Underweight"; }
                    else if (bmi > 20.7 && bmi < 26.4) { result = "In normal range"; }
                    else if (bmi > 26.4 && bmi < 27.8) { result = "Marginally overweight"; }
                    else if (bmi > 27.8 && bmi < 31.1) { result = "Overweight"; }
                    else if (bmi > 31.1 && bmi < 35) { result = "Very Overweight or obese"; }
                    else if (bmi > 35 && bmi < 40) { result = "Severely Obese"; }
                    else if (bmi > 40 && bmi < 50) { result = "Morbidly Obese"; }
                    else { result = "Super Obese"; }

                }
                else
                {
                    Console.WriteLine("Wrong Input!!");
                }

                if (result != null)
                {
                    Console.WriteLine("********************************************");
                    Console.WriteLine("Your BMI result:");
                    Console.WriteLine("Name: " + name);
                    Console.WriteLine("Gender: " + gender);
                    Console.WriteLine("Weight: " + weight);
                    Console.WriteLine("Height: " + height);
                    Console.WriteLine("BMI: " + bmi);
                    Console.WriteLine("Result: " + result);
                    Console.WriteLine("********************************************");
                }

                Console.WriteLine("Hit enter to calculate again or Enter N to exit:");
                exit = Console.ReadLine().ToUpper();
            } while (!exit.Equals("N"));
        }
    }
}



